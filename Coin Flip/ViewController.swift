//
//  ViewController.swift
//  Coin Flip
//
//  Created by Abhishek Kurmi on 15/04/24.
//

import UIKit
import Lottie

class ViewController: UIViewController {
    
    @IBOutlet weak var animationView: LottieAnimationView!
    
    @IBOutlet weak var btn_Coin: UIButton!
    @IBOutlet weak var lbl_HeadOrTail: UILabel!
    
    var isHead: Bool = true

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btn_CoinClicked(_ sender: UIButton) {
        isHead = Bool.random()
        
//        UIView.transition(with: btn_Coin, duration: 0.3, options: .transitionFlipFromLeft, animations: nil, completion: nil)
        
        let coinFlip = CATransition()
        coinFlip.startProgress = 0.0
        coinFlip.endProgress = 1.0
        coinFlip.type = CATransitionType(rawValue: "flip")
        coinFlip.subtype = CATransitionSubtype(rawValue: "fromRight")
        coinFlip.duration = 0.3
        coinFlip.repeatCount = 2
        btn_Coin.layer.add(coinFlip, forKey: "transition")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4){
            if self.isHead{
                self.btn_Coin.setImage(UIImage(resource: .heads), for: .normal)
                self.lbl_HeadOrTail.attributedText = NSAttributedString(string: "Heads ")
            }else{
                self.btn_Coin.setImage(UIImage(resource: .tails), for: .normal)
                self.lbl_HeadOrTail.attributedText = NSAttributedString(string: "Tails ")
            }
            
            self.animationView.isHidden = false
            self.animationView.play(){completed in 
                self.animationView.isHidden = true
            }
            
            
            
        }
        
    }
    


}

